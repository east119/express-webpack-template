var ExtractTextPlugin = require('extract-text-webpack-plugin');
var path = require('path');

console.log('prod...');

var productionConfig = [{
    entry: {
        home: ['./public/home'],
        about: ['./public/about']
    },
    output: {
        filename: './[name]/bundle.js',
        path: path.resolve('./dist'),
        publicPath: '/dist'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loaders: ['babel?presets[]=es2015,presets[]=stage-0,cacheDirectory=true'],
            exclude: /node_modules/
        }, {
            test: /\.(svg|png|jpg|jpeg|gif)$/,
            loader: 'url?limit=8192&context=client&name=[path][name].[ext]'
        }, {
            test: /\.css$/,
            loader: ExtractTextPlugin.extract('style', 'css?sourceMap!resolve-url')
        }, {
            test: /\.scss$/,
            loader: ExtractTextPlugin.extract('style', 'css!resolve-url!sass?sourceMap')
        }]
    },
    plugins: [
        new ExtractTextPlugin('./[name]/index.css', {
            allChunks: true
        })
    ]
}];

module.exports = productionConfig;
