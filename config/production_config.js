/**
 * 生产环境配置信息
 * 这些配置必须都存在（至少key存在）
 */
module.exports = {
    mongodb: {
        secret: 'blog@github.com',
        host: 'localhost',
        port: '27017',
        db: 'blog',
        url: 'mongodb://localhost/blog'
    },
    session: {
        resave: true,
        saveUninitialized: true,
        secret: "blog",
        key: "blog",
        cookie: {
            secure: false,
            maxAge: 1000 * 60 * 60 * 24 * 30  //30天
        }
    },
    log: {
        access: {
            log_format: 'combined',
            logDirectory: '/logs',
            filename: '/access-%DATE%.log',
            date_formate: 'YYYYMMDD',
            frequency: 'daily',
            verbose: false  //详细的
        },
        app: {
            log_config_file: __dirname + '/log4js/prod.json'
        }
    }
};
